package readmegen

import java.io.File
import java.net.HttpURLConnection
import java.net.URL

// TODO create a HTML parser that changes the relative paths from the projecteuler.net website to an absolute path referencing the website
fun main() {
    val problemsDir = File("src/main/kotlin/problems/")
    problemsDir.listFiles()?.forEach {
        val number = it.name.substringAfterLast("problem").toInt()
        println(number)
        println(it.path)
        generateReadme("${it.path}/README.md", number)
    }
}

fun generateReadme(path: String, number: Int) {
    val readme = File(path)
    val description = fetchProblemDescription(number)
    readme.writeText("# <a href=\"https://projecteuler.net/problem=$number\" target=\"_blank\">Problem $number</a>\n\n$description")
}

fun fetchProblemDescription(number: Int): String {
    val url = URL("https://projecteuler.net/minimal=$number")
    val urlConnection = url.openConnection() as HttpURLConnection
    try {
        return urlConnection.inputStream.bufferedReader().readText()
    } finally {
        urlConnection.disconnect()
    }
}