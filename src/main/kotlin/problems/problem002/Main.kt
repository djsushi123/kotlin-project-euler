package problems.problem002

import util.fib

fun main() {
    println(problem002())
}

fun problem002(): Int {
    var i = 0
    var sum = 0
    while (true) {
        val num = fib(i++)
        if (num > 4_000_000) break
        if (num % 2 == 0) sum += num
    }
    return sum
}

