package problems.problem025

import util.bigBigIntSeq
import util.bigIntFibDictionary
import util.takeWhileInclusive

fun main() {
    println(problem025())
}

fun problem025(): Int {
    // populate the dictionary with values that have the lenght up to 1000 (including one single element which has more than 1000)
    val number = bigBigIntSeq.takeWhileInclusive { it.toString().length < 1000 }.last()

    // return the index of that number
    return bigIntFibDictionary.keys.first { bigIntFibDictionary[it] == number } + 1
}
