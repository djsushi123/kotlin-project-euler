package problems.problem019

fun main() {
    println(problem019())
}

fun problem019(): Int {
    // 1 Jan 1901 was a Wednesday
    // day 2 is 1 Jan 1901, because Wednesday is the third day of the week. If n % 7 == x, we know that it's the
    // x-th day of the week (starting at 0).
    var sundays = 0
    var day = 2

    for (year in 1901..2000) {
        months(year.isLeapYear()).forEach {
            day += it
            sundays += if (day % 7 == 6) 1 else 0
        }
    }

    return sundays

}

//                                 Ja  Fe                       Ma  Ap  Ma  Ju  Ju  Au  Se  Oc  No  De
fun months(leap: Boolean) = listOf(31, 28 + if (leap) 1 else 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)

private fun Int.isLeapYear() = this % 4 == 0 && ((this % 100 != 0) xor (this % 400 == 0))