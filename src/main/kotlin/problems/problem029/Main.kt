package problems.problem029

import java.math.BigInteger

fun main() {
    println(problem029())
}

fun problem029(): Int {
    val numbers = mutableSetOf<BigInteger>()

    for (a in 2L..100L) {
        for (b in 2..100) {
            numbers.add(BigInteger.valueOf(a).pow(b))
        }
    }

    return numbers.size
}