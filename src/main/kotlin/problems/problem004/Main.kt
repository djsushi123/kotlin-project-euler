package problems.problem004

import util.isPalindrome

fun main() {
    println(problem004())
}

fun problem004(): Int {
    var largest = 0

    for (x in 100..999) {
        for (y in 100..999) {
            val product = x * y
            if (!product.toString().isPalindrome()) continue
            if (product > largest) largest = product
        }
    }

    return largest
}