package problems.problem016

import java.math.BigInteger

fun main() {
    println(problem016())
}

fun problem016(): Int {
    var n = BigInteger.valueOf(2L)
    repeat(999) { n *= BigInteger.TWO }
    return n.toString().map { it.digitToInt() }.sum()
}
