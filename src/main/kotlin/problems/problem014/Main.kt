package problems.problem014

fun main() {
    println(problem014())
    println(problem014alternative())
}

fun problem014alternative(): Long {
    var longest = 0L
    var terms = 0
    var j: Long

    for (i in 1L..1_000_000L) {
        j = i
        var thisTerms = 1

        while (j != 1L) {
            thisTerms++

            if (thisTerms > terms) {
                terms = thisTerms
                longest = i
            }

            if (j % 2 == 0L) j /= 2
            else j = 3 * j + 1
        }
    }

    return longest
}

fun problem014(): Long {
    // this cache stores the number and the chain length corresponding to that number

    var maxChainLength = 0
    var maxChainLengthNum = 0L
    for (x in 1L until 1_000_000L) {
        val chainLength = x.chainLength()
        if (maxChainLength < chainLength) {
            maxChainLength = chainLength
            maxChainLengthNum = x
        }
    }
    return maxChainLengthNum
}

private var cache = mutableMapOf(
    Pair(1L, 1)
)

private fun Long.chainLength(): Int {
    if (cache.contains(this)) return cache[this]!!

    var x = this
    val workingChainLengths = mutableSetOf<Long>()

    while (true) {
        if (cache.contains(x)) break
        cache[x] = 0
        workingChainLengths.add(x)
        if (x % 2 == 0L) x /= 2
        else x = 3 * x + 1
        workingChainLengths.forEach { cache[it] = cache[it]!! + 1 }
    }

    workingChainLengths.forEach { cache[it] = cache[it]!! + cache[x]!! }
    return cache[this]!!
}