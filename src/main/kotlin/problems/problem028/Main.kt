package problems.problem028

import kotlin.math.floor

fun main() {
    println(problem028())
}

fun problem028(): Int {
    val squareSize = 1001
    return ulamSpiral.take(squareSize / 2 * 4 + 1).sum()
}

private val ulamSpiral = sequence {
    var x = 1
    while (true) {
        yield((floor((x * (x + 2) / 4).toDouble()) + floor(((x % 4) / 3).toDouble()) + 1).toInt())
        x++
    }
}