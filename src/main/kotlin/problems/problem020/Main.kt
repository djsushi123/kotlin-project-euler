package problems.problem020

import util.factorial
import java.math.BigInteger

fun main() {
    println(problem020())
}

fun problem020() = factorial(BigInteger.valueOf(100L)).toString().sumOf { it.digitToInt() }