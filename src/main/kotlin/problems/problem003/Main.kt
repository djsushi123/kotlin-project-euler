package problems.problem003

import util.primeFactors

fun main() {
    println(problem003())
}

fun problem003(): Long {
    val number = 600851475143L
    return primeFactors(number).maxOrNull()!!
}