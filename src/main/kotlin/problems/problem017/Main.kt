package problems.problem017

fun main() {
    println(problem017())
}

// this is just a test
//fun problem017() { (1..1000).forEach { println("$it: ${generateWord(it)} (${countLetters(generateWord(it))})") } }

fun problem017() = (1..1000).map { countLetters(generateWord(it)) }.sum()

private val baseNames = mapOf(
    1 to "one", 2 to "two", 3 to "three", 4 to "four", 5 to "five", 6 to "six", 7 to "seven", 8 to "eight",
    9 to "nine", 10 to "ten", 11 to "eleven", 12 to "twelve", 13 to "thirteen", 14 to "fourteen",
    15 to "fifteen", 16 to "sixteen", 17 to "seventeen", 18 to "eighteen", 19 to "nineteen", 20 to "twenty",
    30 to "thirty", 40 to "forty", 50 to "fifty", 60 to "sixty", 70 to "seventy", 80 to "eighty", 90 to "ninety",
    1000 to "thousand"
)

/**
 * Generates the word form of any number from 0 up to 1000 inclusive.
 */
private fun generateWord(n: Int): String {
    return when (n) {
        in 0..20 -> baseNames[n]!!
        30, 40, 50, 60, 70, 80, 90 -> baseNames[n / 10 * 10]!!
        in 21..99 -> baseNames[n / 10 * 10]!! + "-" + baseNames[n % 10]
        100, 200, 300, 400, 500, 600, 700, 800, 900 -> baseNames[n / 100]!! + " hundred"
        in 100..999 -> baseNames[n / 100]!! + " hundred and " + generateWord(n % 100)
        1000 -> "one thousand"
        else -> throw IllegalArgumentException("The value should be between 1 and 1000. Current value is $n.")
    }
}

/**
 * Counts all the letters in a String.
 */
private fun countLetters(s: String) = s.count { it.isLetter() }