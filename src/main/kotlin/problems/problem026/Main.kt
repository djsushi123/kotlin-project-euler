package problems.problem026

fun main() {
    println(problem026())
}

fun problem026(): Int {
    var maxCycleLength = 0
    var maxD = 0
    for (d in 1 until 1000) {
        findRecurring(1, d).period.size.let {
            if (it > maxCycleLength) {
                maxD = d
                maxCycleLength = it
            }
        }
    }
    return maxD
}

private data class Decimal(
    val n: Int = 0,
    val beforePeriod: MutableList<Int> = mutableListOf(),
    val period: MutableList<Int> = mutableListOf()
)

private fun findRecurring(numerator: Int, denominator: Int): Decimal {
    val number = Decimal(n = numerator / denominator)
    val remainders = mutableListOf<Int>()
    val decimal = mutableListOf<Int>()

    var currentRemainder = numerator
    while (true) {
        currentRemainder %= denominator
        currentRemainder *= 10
        if (currentRemainder in remainders) {
            val startOfCycle = remainders.indexOf(currentRemainder)
            val endOfCycle = remainders.size
            number.period.addAll(decimal.subList(startOfCycle, endOfCycle))
            if (startOfCycle != 0)
                number.beforePeriod.addAll(decimal.subList(0, startOfCycle))
            break
        }
        remainders.add(currentRemainder)
        decimal.add(currentRemainder / denominator)
    }
    return number
}