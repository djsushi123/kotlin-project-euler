package problems.problem005

import util.primeFactors

fun main() {
    println(problem005())
}

/**
 * Goes through all the numbers from 1 to 20 and determines its prime factors. Then, it tries to add them to the main
 * prime factor list. It only adds the numbers that are not yet inside the main prime factor list. After that, it
 * multiplies all the numbers in the main prime factor list and gives the result.
 */
fun problem005(): Long {

    val allFactors = mutableMapOf<Long, Int>()
    for (x in 1L..20L) {
        val factors = primeFactors(x).groupingBy { it }.eachCount()
        factors.forEach { (key, value) ->
            if (allFactors[key] == null) allFactors[key] = 0
            if (allFactors[key]!! < value) allFactors[key] = value
        }
    }
    return allFactors.flatMap { (key, value) ->
        List(value) { key }
    }.reduce(Long::times)
}

