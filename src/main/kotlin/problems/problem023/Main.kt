package problems.problem023

import util.divisors

fun main() {
    println(problem023())
}

fun problem023(): Int {

    val abundants = (12..28123).filter { it.isAbundant() }

    val abundantSums = mutableSetOf<Int>()
    for (x in abundants) {
        for (y in abundants) {
            abundantSums.add(x + y)
        }
    }
    return (1..28123).filter { it !in abundantSums }.sum()
}



private fun Int.isAbundant() = divisors().dropLast(1).sum() > this