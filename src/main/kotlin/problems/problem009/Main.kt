package problems.problem009

fun main() {
    println(problem009())
}

fun problem009(): Int {
    for (a in 1 until 1000) {
        for (b in 1 until 1000) {
            for (c in 1 until 1000) {
                if (a * a + b * b == c * c && a + b + c == 1000) return a * b * c
            }
        }
    }
    return 0
}