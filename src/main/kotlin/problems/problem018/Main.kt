package problems.problem018

fun main() {
    println(problem018())
}

fun problem018(): Int {
    var maxSum = 0

    fun paths(index: Int, currentList: List<Int>) {
        if (currentList.size == 14) {
            with(currentList + listOf(pyramid[currentList.size][index])) {
                if (sum() > maxSum) maxSum = sum()
//                println(this)
                return
            }
        }
        paths(index, currentList + listOf(pyramid[currentList.size][index]))
        paths(index + 1, currentList + listOf(pyramid[currentList.size][index]))
    }

    paths(0, listOf())

    return maxSum
}

// observation: if we label each number from any row starting from index 0, and we have some kind of cursor that
// travels down the pyramid, we can only go down to a number with either the same index or an index + 1
val pyramid = """
        75
        95 64
        17 47 82
        18 35 87 10
        20 04 82 47 65
        19 01 23 75 03 34
        88 02 77 73 07 63 67
        99 65 04 28 06 16 70 92
        41 41 26 56 83 40 80 70 33
        41 48 72 33 47 32 37 16 94 29
        53 71 44 65 25 43 91 52 97 51 14
        70 11 33 28 77 73 17 78 39 68 17 57
        91 71 52 38 17 14 91 43 58 50 27 29 48
        63 66 04 68 89 53 67 30 73 16 69 87 40 31
        04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
    """.trimIndent().lines().map { it.split(" ").map { number -> number.toInt() } }

// used for testing purposes
//val pyramid = listOf(
//    listOf(0),
//    listOf(0, 1)
//)

fun solve() {


}
