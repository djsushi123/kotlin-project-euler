package problems.problem015

import util.factorial
import java.math.BigInteger

fun main() {
    println(problem015())

}

fun problem015() = factorial(BigInteger.valueOf(40L)) / (factorial(BigInteger.valueOf(20L)) * factorial(BigInteger.valueOf(20L)))