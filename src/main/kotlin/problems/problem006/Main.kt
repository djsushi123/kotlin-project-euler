package problems.problem006

fun main() {
    println(problem006())
}

fun problem006() = (1..100).sum() * (1..100).sum() - (1..100).sumOf { it * it }

//fun problem006(): Long {
//    var sumOfSquares = 0L
//    var sum = 0L
//    for (x in 1..100) {
//        sumOfSquares += x * x
//        sum += x
//    }
//    return (sum * sum) - sumOfSquares
//}