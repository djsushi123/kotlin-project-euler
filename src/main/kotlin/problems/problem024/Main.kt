package problems.problem024

import util.factorial

fun main() {
    println(problem024())
}

fun problem024() = listOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).getNthPermutation(999_999).joinToString("")

private fun <T> List<T>.getNthPermutation(n: Long): List<T> {
    val listCopy = toMutableList()
    @Suppress("NAME_SHADOWING")
    var n = n
    val permutation = mutableListOf<T>()

    repeat(size) {
        if (it == size - 1) {
            permutation.add(listCopy.last())
            return@repeat
        }

        val fact = factorial(size - it - 1)
        permutation.add(listCopy[(n / fact).toInt()])
        listCopy.removeAt((n / fact).toInt())
//        println("$permutation: ${size - it - 1}! = $fact")
        n %= fact
    }

    return permutation
}