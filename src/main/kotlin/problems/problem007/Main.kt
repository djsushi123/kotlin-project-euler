package problems.problem007

import util.Generator

fun main() {
    println(problem007())
}

fun problem007() = Generator.primes.take(10_001).last()