package problems.problem027

import util.isPrime

fun main() {
    println(problem027())
}

fun problem027(): Int {
    fun calcNumberOfPrimes(a: Int, b: Int): Int {
        fun Int.eq(a: Int, b: Int) = this * this + this * a + b

        var numOfprimes = 0
        var n = 0
        while (true) {
            if (n.eq(a, b).isPrime()) {
//                println("${n.eq(a, b)} is a prime")
                n++
            }
            else return n
        }
    }

    var maxNumberOfPrimes = 0
    var maxA = 0
    var maxB = 0

    for (a in -999..999) {
        for (b in -1000..1000) {
            calcNumberOfPrimes(a, b).let { numOfPrimes ->
                if (numOfPrimes > maxNumberOfPrimes) {
                    maxNumberOfPrimes = numOfPrimes
                    maxA = a
                    maxB = b
                }
            }
        }
    }

    return maxA * maxB
}