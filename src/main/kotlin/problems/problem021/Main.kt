package problems.problem021

import util.divisors

fun main() {
    println(problem021())
}

fun problem021(): Int {
    val amicables = mutableSetOf<Int>()
    for (x in 1 until 10000) {
        with(x.amicable() ?: continue) {
            amicables.add(x)
            amicables.add(this)
//            println("Amicables: $x - $this")
        }
    }
    return amicables.sum()
}

private fun Int.amicable() = d(this).let { firstSum ->
    firstSum.divisors().dropLast(1).sum().let { secondSum ->
        if (this == secondSum && this != firstSum) firstSum else null
    }
}

private fun d(n: Int) = n.divisors().dropLast(1).sum()

//private fun Int.amicable(): Int? {
//    val otherNumber = factors().sum()
//    return if (otherNumber.factors().sum() == this) otherNumber else null
//}