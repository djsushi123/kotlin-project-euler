package problems.problem012

import util.Generator
import util.divisors
import util.takeWhileInclusive

fun main() {
    println(problem012())
}

fun problem012() = Generator.triangleNumbers.takeWhileInclusive { it.divisors().count() <= 500 }.last()