package problems.problem013

import util.resource
import java.math.BigInteger

fun main() {
    println(problem013())
}

fun problem013() = resource("013", "numbers.txt")
    .readLines()
    .map { BigInteger(it) }
    .let {
        var sum = BigInteger.ZERO
        for (x in it) sum += x
        sum
    }.toString()
    .substring(0, 10)