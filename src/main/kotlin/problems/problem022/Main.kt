package problems.problem022

import util.resource

fun main() {
    println(problem022())
}

val abc = "abcdefghijklmnopqrstuvwxyz"

fun problem022() = resource("022", "names.txt")
    .readText()
    .split(",")
    .sorted()
    .map { it.drop(1).dropLast(1) } // removing quotes
    .mapIndexed { index, s ->
        s.map { c ->
//            println("$index, $s: $c ${abc.indexOf(c.lowercase()) + 1}")
            abc.indexOf(c.lowercase()) + 1
        }.sum() * (index + 1)
    }
    .sum()
