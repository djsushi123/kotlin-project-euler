package problems.problem010

import util.Generator

fun main() {
    println(problem010())
}

fun problem010(): Long {
    return Generator.primes.takeWhile { it < 2_000_000 }.sum()
}