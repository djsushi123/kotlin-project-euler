package problems.problem001

fun main() {
    println(problem001())
}

fun problem001(): Int {
    var sum = 0
    (0..999).forEach {
        if (it % 3 == 0 || it % 5 == 0) sum += it
    }
    return sum
}