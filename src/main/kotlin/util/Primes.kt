package util

fun primeFactors(n: Long): List<Long> {
    if (n == 1L) return listOf(1L)
    var number = n
    val factors = mutableListOf<Long>()
    var x: Long
    while (true) {
        x = 2
        while (true) {
            if (number % x == 0L) {
                factors.add(x)
                number /= x
                break
            }
            x++
        }
        if (number == 1L)
            break
    }
    return factors.toList()
}

fun Int.isPrime(): Boolean {
    if (this <= 1) return false

    var i = 2
    while (i * i <= this) {
        if (this % i == 0) return false
        i++
    }

    return true
}