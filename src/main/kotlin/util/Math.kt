package util

import java.math.BigInteger
import kotlin.math.sqrt

fun factorial(n: BigInteger) = (BigInteger.ONE..n).reduce(BigInteger::times)

fun factorial(n: Long) = (1L..n).reduce(Long::times)

fun factorial(n: Int) = (1..n).reduce(Int::times)

fun Long.pow(exp: Int) = BigInteger.valueOf(this).pow(exp).toLong()

fun Int.divisors(): List<Int> {
    val divisors = mutableListOf<Int>()
    for (x in 1..sqrt(toDouble()).toInt()) {
        if (this % x == 0) {
            divisors.add(divisors.size / 2, x)
            if (this / x != x) divisors.add(divisors.size / 2 + 1, this / x)
        }
    }
    return divisors
    // sorting and converting the factors makes it so much less efficient
//    return factors.sorted().toSet()
}

fun Iterable<Int>.bigIntegerSum(): BigInteger {
    var sum = BigInteger.ZERO
    for (element in this) {
        sum += BigInteger.valueOf(element.toLong())
    }
    return sum
}