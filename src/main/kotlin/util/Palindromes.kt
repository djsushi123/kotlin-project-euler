package util


/**
 * Returns true of the String is palindromic, otherwise false.
 * If we have some number (or String), let's say 9532359, the length is 7. We go through all the outer numbers and compare them
 * one by one. We stop at the numbers that are next to the one in the middle. If we find one mismatch, we return false.
 * If there are no mismatches from index 0 until length / 2, we have a palindrome and return true.
 */
fun String.isPalindrome(): Boolean {
    (0 until length / 2).forEach {
        if (this[it] != this[length - it - 1])
            return false
    }
    return true
}