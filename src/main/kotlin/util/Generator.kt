package util

import kotlin.math.ceil
import kotlin.math.sqrt

object Generator {

    val primes = sequence {
        var current = 3L
        val primeList = mutableSetOf(2L, 3L)
        var square = 2L

        yield(2L)
        yield(3L)

        outer@while (true) {
            current += 2L
            for (x in primeList) {
                if (x > square) break
                if (current % x == 0) {
                    continue@outer
                }
            }
            square = ceil(sqrt(current.toDouble())).toLong()
            primeList.add(current)
            yield(current)
        }
    }

    val triangleNumbers = sequence {
        var counter = 1
        var i = 1
        while (true) {
            yield(i)
            counter++
            i += counter
        }
    }
}

/**
 * Extension of the standard Sequence.takeWhile(). This method acts just as `takeWhile()`, except it also returns the
 * first element that doesn't satisfy the given predicate.
 */
fun <T> Sequence<T>.takeWhileInclusive(predicate: (T) -> Boolean): Sequence<T> {
    var shouldContinue = true
    return takeWhile {
        val result = shouldContinue
        shouldContinue = predicate(it)
        result
    }
}