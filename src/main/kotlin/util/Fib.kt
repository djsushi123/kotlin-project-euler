package util

import java.math.BigInteger

fun fib(i: Int): Int {
    if (fibDictionary.keys.contains(i))
        return fibDictionary[i]!!
    if (i in 0..1)
        return 1
    val result = fib(i - 1) + fib(i - 2)
    fibDictionary[i] = result
    return result
}

private val fibDictionary = mutableMapOf<Int, Int>()


val bigBigIntSeq = sequence {
    var x = 0
    while (true) {
        yield(fibBigInt(x++))
    }
}

fun fibBigInt(i: Int): BigInteger {
    if (bigIntFibDictionary.keys.contains(i))
        return bigIntFibDictionary[i]!!
    if (i in 0..1)
        return BigInteger.ONE
    val result = fibBigInt(i - 1) + fibBigInt(i - 2)
    bigIntFibDictionary[i] = result
    return result
}

val bigIntFibDictionary = mutableMapOf<Int, BigInteger>()