package util

import java.io.File

fun resource(problemNumber: String, filename: String) = File("src/main/resources/problem$problemNumber/$filename")