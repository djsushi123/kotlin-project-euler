package test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import problems.problem001.problem001
import problems.problem002.problem002
import problems.problem003.problem003
import problems.problem004.problem004
import problems.problem005.problem005
import problems.problem006.problem006
import problems.problem007.problem007
import problems.problem008.problem008
import problems.problem009.problem009
import problems.problem010.problem010
import problems.problem011.problem011
import problems.problem012.problem012
import problems.problem013.problem013
import problems.problem014.problem014
import problems.problem014.problem014alternative
import problems.problem015.problem015
import problems.problem016.problem016
import problems.problem017.problem017
import problems.problem018.problem018
import problems.problem019.problem019
import problems.problem020.problem020
import problems.problem021.problem021
import problems.problem022.problem022
import problems.problem023.problem023
import problems.problem024.problem024
import problems.problem025.problem025
import problems.problem026.problem026
import problems.problem027.problem027
import problems.problem028.problem028
import problems.problem029.problem029
import java.math.BigInteger

internal class SolutionTests {

    @Test
    fun problem001test() {
        assertEquals(233168, problem001())
    }

    @Test
    fun problem002test() {
        assertEquals(4613732, problem002())
    }

    @Test
    fun problem003test() {
        assertEquals(6857, problem003())
    }

    @Test
    fun problem004test() {
        assertEquals(906609, problem004())
    }

    @Test
    fun problem005test() {
        assertEquals(232792560, problem005())
    }

    @Test
    fun problem006test() {
        assertEquals(25164150, problem006())
    }

    @Test
    fun problem007test() {
        assertEquals(104743, problem007())
    }

    @Test
    fun problem008test() {
        assertEquals(23514624000, problem008())
    }

    @Test
    fun problem009test() {
        assertEquals(31875000, problem009())
    }

    @Test
    fun problem010test() {
        assertEquals(142913828922, problem010())
    }

    @Test
    fun problem011test() {
        assertEquals(70600674, problem011())
    }

    @Test
    fun problem012test() {
        assertEquals(76576500, problem012())
    }

    @Test
    fun problem013test() {
        assertEquals("5537376230", problem013())
    }

    @Test
    fun problem014test() {
        assertEquals(837799, problem014())
    }

    @Test
    fun problem014testAlternative() {
        assertEquals(837799, problem014alternative())
    }

    @Test
    fun problem015test() {
        assertEquals(BigInteger.valueOf(137846528820L), problem015())
    }

    @Test
    fun problem016test() {
        assertEquals(1366, problem016())
    }

    @Test
    fun problem017test() {
        assertEquals(21124, problem017())
    }

    @Test
    fun problem018test() {
        assertEquals(1074, problem018())
    }

    @Test
    fun problem019test() {
        assertEquals(171, problem019())
    }

    @Test
    fun problem020test() {
        assertEquals(648, problem020())
    }

    @Test
    fun problem021test() {
        assertEquals(31626, problem021())
    }

    @Test
    fun problem022test() {
        assertEquals(871198282, problem022())
    }

    @Test
    fun problem023test() {
        assertEquals(4179871, problem023())
    }

    @Test
    fun problem024test() {
        assertEquals("2783915460", problem024())
    }

    @Test
    fun problem025test() {
        assertEquals(4782, problem025())
    }

    @Test
    fun problem026test() {
        assertEquals(983, problem026())
    }

    @Test
    fun problem027test() {
        assertEquals(-59231, problem027())
    }

    @Test
    fun problem028test() {
        assertEquals(669171001, problem028())
    }

    @Test
    fun problem029test() {
        assertEquals(9183, problem029())
    }

}